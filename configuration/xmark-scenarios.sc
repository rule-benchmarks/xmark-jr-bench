queries 	=  ../queries/xmark-queries.dlgp
queries_IDB 	=  ../queries/xmark-queries_IDB.dlgp
atomic 		=  ../queries/atomicquery.dlgp
atomic_IDB	=  ../queries/atomicquery_IDB.dlgp


#  10M
## R10

scenario.10M_R10.data			=  	../10M_R10/conf.rls
scenario.10M_R10.rules 	  		=  	../rules/10/R10.dlgp
scenario.10M_R10.workload   	= 	${atomic} 


#  10M
## R100

scenario.10M_R100.data			=  	../10M_R100/conf.rls
scenario.10M_R100.rules 	  	=  	../rules/100/R100.dlgp
scenario.10M_R100.workload   	= 	${queries} 


#  10M
## R500

scenario.10M_R500.data			=  	../10M_R500/conf.rls
scenario.10M_R500.rules 	  	=  	../rules/500/R500.dlgp
scenario.10M_R500.workload   	= 	${queries} 

